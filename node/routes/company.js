const express = require('express');
const router = express.Router();
const companyController = require('./../controllers/company.controller');
const multer = require('multer');


var storage = multer.diskStorage({
    destination: function(req, file, cb) {
        cb(null, './uploads')
    },
    filename: function(req, file, cb) {
        cb(null, Date.now() + file.originalname);
    }
})

var upload = multer({ storage: storage })


router.post('/signup', companyController.signupCompany);
router.post('/login', companyController.loginCompany);
router.post('/verifyEmail', companyController.verifyEmail);
router.post('/updateCompany', companyController.updateCompany);
router.post('/logo', upload.single('logo'), (req, res, next) => {
    res.json({ error: false, path: 'http://localhost:3000/' + req.file.path });
})
module.exports = router;