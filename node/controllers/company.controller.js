const companyModel = require("../models/company.model").companyModel;
const sendmail = require('./email.controller');
const signupCompany = (req, res, next) => {
    let email = req.body.email;
    let password = req.body.password;
    let name = req.body.name;
    let newCompany = new companyModel({
        name: name,
        email: email,
        password: password,
    });
    newCompany.save((err, doc) => {
        if (err) {
            res.status(400).json({ error: true, message: err });
        } else {
            delete doc["password"];
            sendmail(doc.email, 'http://localhost:4200/auth?emailVerification=true&id=' + doc._id).then(sent => {
                //http://localhost:4200/auth?emailVerification=true&id=something
                res
                    .status(201)
                    .json({ error: false, message: "created company", data: doc, emailSent: true });
            }).catch(
                err => {
                    console.log(err);
                    res
                        .status(201)
                        .json({ error: false, message: "created company", data: doc, emailSent: false, reason: err });
                });
        }
    });
};

const loginCompany = (req, res, next) => {
    let email = req.body.email;
    let password = req.body.password;
    companyModel.find({ email: email, password: password },
        "email _id name logo isEmailVerified",
        (err, doc) => {
            if (err) {
                res.status(401).json({ error: true, message: err });
            } else {
                // console.log(doc);
                if (doc.length >= 1) {
                    res
                        .status(200)
                        .json({ error: false, message: "user found", data: doc });
                } else {
                    res
                        .status(401)
                        .json({ error: true, message: "user Not found", data: doc });
                }
            }
        }
    );
};


const verifyEmail = (req, res, next) => {
    let companyId = req.body.id;
    companyModel.findByIdAndUpdate(companyId, { isEmailVerified: true }).then(result => {
        // console.log(result);
        result.isEmailVerified = true;
        res
            .status(200)
            .json({ error: false, message: "verification successful", data: result });
    }).catch(err => {
        console.log(err);
        res
            .status(400)
            .json({ error: true, message: "Email verification error", data: err });
    })
}


const updateCompany = (req, res, next) => {
    let companyId = req.body._id;
    delete req.body._id;
    // console.log(req.body);
    companyModel.findByIdAndUpdate(companyId, req.body).then(result => {
        // console.log(result);
        res
            .status(200)
            .json({ error: false, message: "Update successful", data: req.body });
    }).catch(err => {
        console.log(err);
        res
            .status(400)
            .json({ error: true, message: "error", data: err });
    })
}
module.exports = {
    signupCompany,
    loginCompany,
    verifyEmail,
    updateCompany
};