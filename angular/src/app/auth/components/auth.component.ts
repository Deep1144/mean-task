import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import {AuthService} from './../services/auth.service';
import { Router, ActivatedRoute } from '@angular/router';
@Component({
  selector: 'app-auth',
  templateUrl: './auth.component.html',
  styleUrls: ['./auth.component.scss']
})

export class AuthComponent implements OnInit {
  formEmail :string ='';
  formPassword : string ='';
  formConfirmPassword : string ='';
  isLoginForm :boolean=true ;
  companyName : string = '';
  loginError :  boolean = false;
  nameAlreadyTakenError : boolean = false;
  isMessage :boolean = false;
  message :string = '';
  isLoading :boolean =false;

  constructor(private authService : AuthService ,private router :Router ,private route: ActivatedRoute) { }
  
  ngOnInit(): void {
    if(this.route.snapshot.queryParamMap){
      if(this.route.snapshot.queryParamMap.get('emailVerification')){
        let companyId = this.route.snapshot.queryParamMap.get('id');
        if(companyId){
          this.authService.verifyEmail(companyId).subscribe(res=>{
            this.authService.currentCompany = res['data'];
            localStorage.setItem('company' , JSON.stringify(this.authService.currentCompany ));
            this.router.navigate(['/dashboard']);
          },err=>{
            this.isMessage = true;
            this.message = "encountered some problem please try after some time";
          });
        } 
      }else{
        let localDetails = localStorage.getItem('company');
        if(localDetails){
          this.authService.currentCompany = JSON.parse(localDetails);
          this.router.navigate(['/dashboard']); 
        }      
      }
    }
  }

  submitForm(form : NgForm){
    form.value.email = form.value.email.toLowerCase();
    if(this.isLoginForm){
      this.authService.login(form.value).subscribe(res=>{
        this.authService.currentCompany = res['data'][0];
        localStorage.setItem('company' ,JSON.stringify( this.authService.currentCompany));
        this.router.navigate(['dashboard']);     
      },err=>{
        console.log(err)
        this.loginError=true;
      }); 
    }else {
      if(this.formConfirmPassword != this.formPassword){
        alert('passwords shold match');
        return; 
      }
      this.isLoading = true;
      this.authService.signup(form.value).subscribe(res=>{
        // this.authService.currentCompany = res['data'];
        this.authService.currentCompany = res['data'];
        // this.router.navigate(['dashboard']);    
        // console.log(this.authService.currentCompany);
        localStorage.setItem('company' ,JSON.stringify(this.authService.currentCompany));
        if(res['emailSent']){
          this.isMessage = true;
          this.message = 'An Authentication mail has been sent to your email ,please verify your email';
        }else{
          this.isMessage = true;
          this.message = 'Account is created but we faced some issue while sending verification email';
        }
        this.isLoading =false;
      },(err)=>{
        this.nameAlreadyTakenError = true;
      })
    }
  }
}
