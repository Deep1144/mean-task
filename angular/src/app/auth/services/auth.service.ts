import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {baseUrl} from '../../../environments/environment';
@Injectable({
  providedIn: 'root'
})
export class AuthService {

  constructor(private http : HttpClient) { }
  currentCompany : any = {
    // email : 'deep1@gmndkji.com',
    // name : "deep",
    // _id : '5f5761f7b5f20f33c8f26367',
    // logo : ''
  };

  signup(form){
    let obj ={
      email : form.email , password : form.password,
      name : form.companyName 
    }
   return this.http.post(baseUrl+'company/signup' , obj);
  }
  login(form){  
    let obj = {
      email : form.email,
      password : form.password
    }
    return this.http.post(baseUrl +'company/login',{email : form.email , password : form.password});
  }

  verifyEmail(id){
    return this.http.post(baseUrl +'company/verifyEmail',{id:id});
  }
}
