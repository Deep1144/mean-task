import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import { baseUrl } from 'src/environments/environment';
import { Employee } from '../models/employee.model';
@Injectable({
  providedIn: 'root'
})
export class EmployeeService {

  constructor(private http : HttpClient) { }

  addEmployee(data:Employee){
    console.log(data);
    return this.http.post(baseUrl + 'employees/add-employee' , data);
  }

  getEmployee(companyId : String){
    return this.http.get(baseUrl +'employees/get-employee/'+companyId)
  }

  updateEmployee(data:Employee){
    return this.http.post(baseUrl + 'employees/update-employee' , data );
  }

  deleteEmployee(id){
    return this.http.delete(baseUrl + 'employees/delete-employee/'+id);
  }


  uploadImage(fd){
    return this.http.post(baseUrl+ 'employees/upload-image',fd);
  }
}
