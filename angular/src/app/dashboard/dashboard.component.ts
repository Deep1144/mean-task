import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { AuthService } from '../auth/services/auth.service';
import { Employee } from './models/employee.model';
import { MatChipInputEvent } from '@angular/material/chips/chip-input';
import { FormControl, NgForm } from '@angular/forms';
import { EmployeeService } from './services/employee.service';
import { MatTableDataSource } from '@angular/material/table';
import { MatSnackBar } from '@angular/material/snack-bar';
import { MatDialog } from '@angular/material/dialog';
import { DialogComponent } from './helper/dialog/dialog.component';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss'],
})
export class DashboardComponent implements OnInit {
  @ViewChild('hobbiInput') hobbiInput: ElementRef<HTMLInputElement>;
  @ViewChild('newEmployeeForm') employeeForm: NgForm;
  companyDetails;
  separatorKeysCodes: number[] = [13, 188];
  employeesData: Employee[] = [];
  newEmployee: Employee = {
    _id: '',
    companyId: '',
    name: '',
    contactNo: '',
    address: {
      city: '',
      countary: '',
      state: '',
    },
    hobbies: [],
    dateOfBirth: new Date(),
    dateOfJoin: new Date(),
    profilePhoto: '/some/photo',
  };
  displayedColumns: string[] = ['name','contactNo','city','state','countary','hobbies','dateOfBirth','dateOfJoin','profilePhoto','edit','delete',];
  dataSource;
  image; imageToSend: FormData = new FormData();  imageSrc: any;


  constructor(protected authService: AuthService,public dialog :MatDialog,protected employeeService: EmployeeService,private snackBar: MatSnackBar) { }
  
  ngOnInit(): void {
    this.companyDetails = this.authService.currentCompany;
    this.getEmployee();
  }

  openDialog(){
    let metref = this.dialog.open(DialogComponent , {
      width  : '350px',
      data : {companyDetails : this.authService.currentCompany}
    })

    metref.afterClosed().subscribe(res=>{
      this.authService.currentCompany = res['res'];
      this.companyDetails = res['res']
    })
  }

  addHobbies(event: MatChipInputEvent): void {
    const input = event.input;
    const value = event.value;
    if ((value || '').trim()) {
      this.newEmployee.hobbies.push(value.trim());
    }
    if (input) {
      input.value = '';
    }
    this.hobbiInput.nativeElement.innerText = ' ';
  }

  addEmployee(form?: NgForm) {
    this.newEmployee['companyId'] = this.authService.currentCompany['_id'];
    if (this.image) {
      this.uploadImage()
        .then((res: string) => {
          this.newEmployee.profilePhoto = res;
        })
        .finally(() => {
          uploaddata.bind(this)();
        });
    } else {
      uploaddata.bind(this)();
    }

    function uploaddata() {
      if (this.newEmployee._id) {
        this.employeeService
          .updateEmployee(this.newEmployee)
          .subscribe((res) => {
            console.log(res);
            this.openSnackBar('Updated ' + this.newEmployee.name);
            this.getEmployee();
            this.employeeForm.resetForm();
            this.imageToSend.delete('image');
            this.image = '';
            this.imageSrc ='';
          });
      } else {
        this.employeeService.addEmployee(this.newEmployee).subscribe((res) => {
          this.openSnackBar('Added ' + this.newEmployee.name);
          this.getEmployee();
          this.employeeForm.resetForm();
          this.imageToSend.delete('image');
          this.image ='';
          this.imageSrc ='';
        });
      }
    }
  }

  getEmployee() {
    this.employeeService
      .getEmployee(this.authService.currentCompany['_id'])
      .subscribe(
        (res) => {
          this.employeesData = res['data'];
          this.dataSource = new MatTableDataSource(this.employeesData);
          console.log(this.dataSource);
        },
        (err) => {
          console.log(err);
        }
      );
  }

  readFile(event) {
    const reader = new FileReader();
    reader.onload = (e) => (this.imageSrc = reader.result);
    reader.readAsDataURL(event.target.files[0]);
    this.image = event.target.files[0];
  }

  deleteButtonClick(employee: Employee) {
    if (confirm('Are you sure you want to delete ' + employee.name)) {
      this.employeeService.deleteEmployee(employee._id).subscribe((res) => {
        this.openSnackBar('Deleted ' + employee.name);
        this.getEmployee();
      });
    }
  }

  updateButtonClick(employee: Employee) {
    console.log(employee);
    this.newEmployee = Object.assign({}, employee);
  }

  openSnackBar(content: string = '') {
    this.snackBar.open(content, 'ok', { duration: 1.5 * 1000 });
  }

  uploadImage() {
    return new Promise((resolve, reject) => {
      if (this.image) {
        this.imageToSend.append('image', this.image);
        this.employeeService.uploadImage(this.imageToSend).subscribe(
          (res) => {
            resolve(res['path']);
          },
          (err) => {
            reject(err);
            console.log(err);
          }
        );
      } else {
        reject();
      }
    });
  }
}
