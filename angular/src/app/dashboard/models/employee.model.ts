export interface Employee { 
    companyId :string,
    _id :string,
    name :string,
    dateOfJoin : Date,
    dateOfBirth :Date,
    profilePhoto :string,
    contactNo : string,
    address: {
      city :string,
      state :string,
      countary : string
    },
    hobbies : Array<String>
}