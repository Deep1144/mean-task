import { Component, Inject, OnInit } from '@angular/core';
import {
  MatDialog,
  MatDialogRef,
  MAT_DIALOG_DATA,
} from '@angular/material/dialog';
import { HttpClient } from '@angular/common/http';
import { baseUrl } from 'src/environments/environment';
import { MatSnackBar } from '@angular/material/snack-bar';
import { Router } from '@angular/router';

@Component({
  selector: 'app-dialog',
  templateUrl: './dialog.component.html',
  styleUrls: ['./dialog.component.scss'],
})
export class DialogComponent implements OnInit {
  companyDetails;
  avtarSource;
  logoData = new FormData();
  constructor(public dialogRef: MatDialogRef<DialogComponent>,@Inject(MAT_DIALOG_DATA) public data ,private http:HttpClient,private snackbar :MatSnackBar,private router :Router) { 
    this.companyDetails =Object.assign({}, data.companyDetails);
    dialogRef.backdropClick().subscribe(res=>{
      // console.log(res);
      dialogRef.close({res : this.companyDetails});
    })
    if(this.companyDetails.logo){
      this.avtarSource = this.companyDetails.logo;
    }else{
      this.avtarSource = './../../../../assets/avtar.jpg';
    }
  }

  ngOnInit(){
    // console.log(this.data.companyDetails);
  }

  logoChange(event){
    console.log(event.target.files[0]);
    let file = event.target.files[0];
    let logo = new FileReader();
    logo.onload = (e)=>{
      this.avtarSource = logo.result;
    }
    logo.readAsDataURL(file);
    this.logoData.append('logo',file);
    this.http.post(baseUrl +'company/logo' , this.logoData).subscribe(res=>{
      console.log(res);
      this.snackbar.open("Logo updated","ok");
      this.logoData.delete('logo');
      this.companyDetails.logo =  res['path'];
      this.http.post(baseUrl + 'company/updateCompany' ,this.companyDetails).subscribe(res=>{
        // console.log(res);
        this.snackbar.open("updated" ,"ok",{duration : 1.0*1000});
      },err=>{console.log(err)}); 
    },err=>console.log(err))  
  }

  updateCompanyData(event){
    if(event.keyCode == 13  ){
      this.http.post(baseUrl + 'company/updateCompany' ,this.companyDetails).subscribe(res=>{
        this.snackbar.open("updated" ,"ok",{duration : 1.0*1000});
      },err=>{console.log(err)});
    }
  }

  logout(){
    localStorage.clear();
    this.dialogRef.close();
    this.router.navigate(['/auth'])
  }
}
